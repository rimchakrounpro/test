//Imports
const authService=  require("./authService")
const dotenv = require("dotenv");

// Constants
const { validationResult } = require('express-validator');
module.exports = {
 register: async(req, res,next)=> {
     try {
      const error = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

      if (!error.isEmpty()) {
        res.status(422).json({ success:false,msg: error.array()[0].msg});
        return;
      }
      authService.register(req.body).then(result =>
      res.status(result.status).send(result.payload))
    }catch(err) {
     return next(err)
   }
  },
   login: function(req, res) {
  
      
    
   try {
      const error = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

      if (!error.isEmpty()) {
        res.status(422).json({ success:false,msg: error.array()[0].msg});
        return;
      }
     authService.login(req.body).then(result =>{
      res.cookie('access_token', result.payload.token, {
      httpOnly: false,
      // secure: true,
      maxAge: process.env.JWT_EXPIRE_IN
    });


      res.status(result.status).send(result.payload)})}
      catch(err) {
     return next(err)
      
  }},
  };
 


