//Imports
const express = require("express");
const authController = require("./authController");
const validation=require("../../middelware/validation")
const authRouter = express.Router();
authRouter.route("/register").post(validation.validate('register'),authController.register);
authRouter.route("/login").post(validation.validate('login'),authController.login);

module.exports = authRouter;

