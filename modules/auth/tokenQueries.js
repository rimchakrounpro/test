 const connexion = require("../../config/database");
module.exports = {
    create:(data)=>{
             let sql=
      "INSERT INTO tokens (user, token, expire_at) values(?,?,?)";
      return new Promise((resolve, reject) => {
      connexion.query(sql, [data.userId, data.token, data.expiresAt] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
    },
}