//Imports
const express = require("express");
const commentController = require("./commentController");
const auth=require("../../middelware/auth")
const path = require("path");
const commentRouter = express.Router();
commentRouter.route("/").post(auth,commentController.store);
commentRouter.route("/post/:id").get(auth,commentController.getCommentsByPost);
commentRouter.route("/:id/user/:user").delete(auth,commentController.delete);
module.exports =  commentRouter;
