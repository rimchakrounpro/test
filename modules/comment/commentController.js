//Imports
const commentService=  require("./commentService")
const jwtUtils=require("../../utils/jwt.utils")

//Routes
module.exports = {
  store:function(req,res){
  commentService.store(req.body,req.idUser).then(result =>
      res.status(result.status).send(result.payload))
},
getCommentsByPost: (req,res) =>{  
  idPost=req.param("id");
  commentService.getCommentsByPost(idPost).then(result =>
      res.status(result.status).send(result.payload))
},
delete:(req,res)=>{

  idComment=req.param("id");
  //id prop video
  idUserVideo=req.param("user");
  //get id user 
  headerAuth = req.headers["authorization"];
  idUser = jwtUtils.getUserId(headerAuth);

  commentService.delete(idComment,idUserVideo,idUser).then(result =>
      res.status(result.status).send(result.payload))
},
};
