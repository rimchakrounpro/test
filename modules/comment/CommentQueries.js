 const connexion = require("../../config/database");
module.exports = {
    create:(comment)=>{
             let sql=
      "INSERT INTO comments (content, posts_id_post, users_id_user) values(?,?,?)";
      return new Promise((resolve, reject) => {
      connexion.query(sql, [comment.content, comment.id_post, comment.idUser] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
    },

deleteByVideo:(idPost)=>{

    let sqlQuery= "DELETE FROM comments WHERE id_post= ?"  ;

     return new Promise((resolve, reject) => {
            connexion.query(sqlQuery,[idPost],(err, rows) => {
             
                if (err) {
                    reject(err)}
                    else
                resolve(true)
            })
        })
},

getCommentsByPost:(idPost)=>{
    
    let sql =
      "SELECT comments.posts_id_post as id_post, users.image,users.id_user,users.firstname, users.lastname, comments.content, comments.id_comment, comments.created_at FROM comments inner join users on users.id_user = comments.users_id_user WHERE comments.posts_id_post=? ORDER BY comments.created_at DESC";

    return new Promise((resolve, reject) => {
      connexion.query(sql,[idPost] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
},
delete:(idComment,idUserVideo, idUser)=>{
     sql = "DELETE FROM comments WHERE id_comment = ? AND users_id_user=? OR users_id_user=?"
    return new Promise((resolve, reject) => {
      connexion.query(sql,[idComment,idUserVideo,idUser] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
}
}
 