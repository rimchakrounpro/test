//Imports
const commentQueries=require("./CommentQueries")
//Routes
module.exports = {
  store: async  (data,idUser) =>{
    let comment={
      content:data.content,
      idUser : idUser,
      id_post : data.id_post
	}
	    return commentQueries.create(comment).then(response => ({
				status: 200,
				payload: { success: true, msg:"Enregistré avec succes "}
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  },
  getCommentsByPost:async(id)=>{
      return commentQueries.getCommentsByPost(id).then(response => ({
				status: 200,
				payload: { success: true, data: response }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  },
  delete:async(idComment,idUserVideo,idUser)=>{
    return commentQueries.delete(idComment,idUserVideo,idUser).then(response => ({
				status: 200,
				payload: { success: true, msg:"supprimé avec success" }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			})); 
  },
 
};
