//Imports
const trainingService=  require("./trainingService")
const jwtUtils=require("../../utils/jwt.utils")
const arrayQuestions=require('../../config/questions');
const fs=require("fs")


//Routes
module.exports = {
  store:function(req,res){

   
    // verifier si la personne a ajouter un fichier
    // Virify if file wase added
    if(req.file===undefined){
      return res.status(400).json({ success:false,
        msg: 'La vidéo est obligatoire!'
      });
    }
    //  verifier si l'utilisateur a choisi une question parmi les question proposé 
    // verify if user select a qustion from proposed list
    if (!arrayQuestions.includes(req.body.question)){
      return res.status(400).json({ success:false,
        msg: 'La question est obligatoire est doit etre parmi les choix proposés'
      });
    }

    // Creer un objet video à enregistrer dans la base de données
    // Create an object video to save it in data base

    const name=req.file.filename;
    const training={
        question:req.body.question,
        videoname: name,
        idUser:req.idUser,
    }
    
    
    trainingService.store(training).then(result =>
      res.status(result.status).send(result.payload)).catch(err=>{
     // delete file async   
      fs.unlink(process.cwd()+ "/public/videos/"+name)        
      });
 
},
getById: (req,res) =>{  
  id=req.param("id");
  trainingService.getById(id).then(result =>
      res.status(result.status).send(result.payload))
},

};
