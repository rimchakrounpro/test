//Imports
const trainingQueries=require("./trainingQueries")
const postQueries=require("../post/postQueries")
//Routes



module.exports = {
  store: async  (training) =>{
let idpub=null;
await postQueries.create({type:'training',idUser:training.idUser}).then(response=>{
	 idpub=response	   
 });
 if(idpub==null){
	 return {
				status: 400,
				payload: { success: false, msg: "erreur insersion post" }
 }}
  training={...training,idpub}
	  return trainingQueries.create(training).then(response => ({
				status: 200,
				payload: { success: true, msg:"Enregistré avec succes "}
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));

  },
  getById:async(id)=>{
      return trainingQueries.findOne(id).then(response => ({
				status: 200,
				payload: { success: true, data: response }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  },
 
};
