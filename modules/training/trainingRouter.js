//Imports
const express = require("express");
const trainingController = require("./trainingController");
const multerMiddelware=require("../../middelware/multer")
const auth=require("../../middelware/auth")
const trainingRouter = express.Router();
trainingRouter.route("/").post(auth,multerMiddelware.multVideo,trainingController.store);
trainingRouter.route("/:id").get(auth,trainingController.getById);
module.exports =  trainingRouter;
