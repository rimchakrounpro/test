 const connexion = require("../../config/database");
module.exports = {
    create:(training)=>{
            let sql=
      "INSERT INTO trainings (id_training, question,video_name) values(?,?,?)";
      return new Promise((resolve, reject) => {
      connexion.query(sql, [training.idpub, training.question, training.videoname] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
    },

findOne:(id)=>{
    
    let sql =
      "SELECT id_training, question,video_name as videoname FROM  trainings where id_training=?";

    return new Promise((resolve, reject) => {
      connexion.query(sql,[id] , (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
},


}
 