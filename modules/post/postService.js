//Imports
const postQueries=require("./postQueries")
const commentQueries=require("../comment/CommentQueries")
const statusQueries=require("../status/statusQueries")
const trainingQueries=require("../training/trainingQueries")


const jwtUtils= require("../../utils/jwt.utils")
const fs=require("fs")

//Routes
module.exports = {

  /**store*/
  store: (video) =>{  
  return postQueries.create(video).then(response => ({
				status: 200,
				payload: { success: true, msg:"Enregistré avec success" }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			})); 
  },

  getVideosByUser:(idUser, res)=>{
     return postQueries.getVideosByUser(idUser).then(response => ({
				status: 200,
				payload: { success: true, data: response }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  },
  delete:async(idVideo,idUser)=>{
	  
   let post=null;
	await postQueries.getVideo(idVideo).then(data=>{
		post=data;
	  })

	   if (idUser!==null&&post.users_id_user!==idUser){
			  return {status:400,payload:{ success: false, msg: "Action non autorisée!" }}
			}
	
			if (post.type=="training")
			{
				await trainingQueries.findOne(post.id_post).then(data=>{
					console.log("le post à supprimer est de type training :", data.videoname)
					 fs.unlink(process.cwd()+ "/public/videos/"+data.videoname,
					 (err => { 
							 if (err) console.log("erreur de supp de fichier !!!!!!!!!!!!"); 
							 else { 
								 console.log("fihchier supp avec success"); 
								} }))

				})

			}
			else if(post.type=="status")
			{
				await statusQueries.findOne(post.id_post).then(data=>{
					console.log("le post à supprimer est de type status :", data.typename)
					if( data.type=='video')
					{
						 fs.unlink(process.cwd()+ "/public/videos/"+data.typename, (err => { 
							 if (err) console.log(err); 
							 else { 
								 console.log("\nDeleted Symbolic Link: symlinkToFile"); 
								} }))
					}
					else if (data.type=='image')
					{
						fs.unlink(process.cwd()+ "/public/images/"+data.typename,(err => { 
							 if (err) console.log(err); 
							 else { 
								 console.log("\nDeleted Symbolic Link: symlinkToFile"); 
								} }) )

					}
					
				})

			}
		
			  return postQueries.delete(idVideo).then(response => ({
				  status: 200,
				  payload: { success: true, msg:"Supprimé avec success" }
			}))
		
  },
  all:(res)=>{
     return postQueries.all(res).then(response => ({
				status: 200,
				payload: { success: true, data: response }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  }
};
