//Imports
const postService=  require("./postService")
const jwtUtils=require("../../utils/jwt.utils")
const fs=require("fs")
const jwt = require('jsonwebtoken');
const arrayQuestions=require('../../config/questions');
//Routes
module.exports = {

  store:function(req,res){
    const test= req.body;
  
    // verifier si la personne a ajouter un fichier
    if(req.file===undefined){
      return res.status(400).json({ success:false,
        msg: 'La vidéo est obligatoire!'
      });
    }
    //  verifier si l'utilisateur a choisi une question parmi les question proposé 
    if (!arrayQuestions.includes(req.body.title)){
      return res.status(400).json({ success:false,
        msg: 'La question est obligatoire est doit etre parmi les choix proposés'
      });
    }

    // Creer un objet video à enregistrer dans la base de données
    const extension=req.file.filename.split('.').pop();


    const name=req.file.filename;
    const video={
        title:req.body.title,
        name: name,
        idUser:req.param("id"),
        extension:extension
    }
    // passer l'objet video au service 
    postService.store(video).then(result =>
      res.status(result.status).send(result.payload)).catch(err=>{
     // delete file async   
      fs.unlink(process.cwd()+ "/public/videos/"+name)        
      });

},
getVideosByUser: (req,res) =>{  
  idUser=req.param("id");
  postService.getVideosByUser(idUser).then(result =>
      res.status(result.status).send(result.payload));
},


delete:(req,res)=>{
  idpost=req.param("id");

  postService.delete(idpost,req.idUser).then(result =>{
     res.status(result.status).send(result.payload)
  }
     );
},
all:(req,res)=>{
  console.log("**********hiiiiiiiiiiiiiii")
  postService.all(res).then(result =>{
      res.status(result.status).send(result.payload)});
},
getVideo: (req, res)=>{
 


 
try{
   // This line opens the file as a readable stream
  var readStream = fs.createReadStream(process.cwd()+ "/public/videos/" + req.param('id'));

  // This will wait until we know the readable stream is actually valid before piping
  readStream.on('open', function () {
    // This just pipes the read stream to the response object (which goes to the client)
    readStream.pipe(res);
  });

  // This catches any errors that happen while creating the readable stream (usually invalid names)
  readStream.on('error', function(err) {
    res.end(err);
  });

}
catch(e){
 res.send({ success: false, msg:e })
}

  },
};

