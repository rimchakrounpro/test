//Imports
const express = require("express");
const postController = require("./postController");
const auth=require("../../middelware/auth")
const multerMiddelware=require("../../middelware/multer")
const postRouter = express.Router();



postRouter.route("/").get(auth,postController.all);//checked

postRouter.route("/video/:id").get(postController.getVideo);


postRouter.route("/user/:id").get(auth,postController.getVideosByUser);//checked

postRouter.route("/:id").delete(auth,postController.delete);









module.exports =  postRouter;
