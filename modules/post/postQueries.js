 const connexion = require("../../config/database");
module.exports = {
all:(res)=>{
 let sql = "SELECT users.image, users.firstname,users.lastname, users.id_user,posts.id_post,posts.created_at,posts.nb_like,posts.nb_report, posts.type FROM posts inner join users on users.id_user = posts.users_id_user ORDER BY posts.created_at DESC";
   return new Promise((resolve, reject) => {
      connexion.query(sql, (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
},
create:(post)=>{
let sql =
      "INSERT INTO posts (type, users_id_user) values(?,?)";
      return new Promise((resolve, reject) => {
      connexion.query(sql,
      [post.type,post.idUser] , (err, rows) => {
        if (err)  {
          reject(err)};
          console.log("id post ",rows.insertId)
        resolve(rows.insertId);
      });
    });
    },
    getVideo:(id)=>{
      const sql="SELECT * FROM posts WHERE id_post=?"
      return new Promise((resolve, reject) => {
      connexion.query(sql,
      [id] , (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
    },
getVideosByUser:(idUser)=>{
 let sql =
      "SELECT * FROM posts inner join users on users.id_user = posts.users_id_user  WHERE posts.users_id_user=? ORDER BY posts.created_at DESC";
   return new Promise((resolve, reject) => {
      connexion.query(sql,
      [idUser] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
    }
    ,
delete:(idpub)=>{
         let sql= "DELETE FROM posts WHERE id_post =?";
     return new Promise((resolve, reject) => {
      connexion.query(sql,
      [idpub] , (err) => {
        if (err) reject(err);
        resolve(true);
      });
    });
    }
}
 