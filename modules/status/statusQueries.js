 const connexion = require("../../config/database");
module.exports = {
    create:(status)=>{
            let sql=
      "INSERT INTO status (id_status,content,type,type_name) values(?,?,?,?)";
      return new Promise((resolve, reject) => {
      connexion.query(sql, [status.idpub, status.content, status.type,status.typename] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
    },

findOne:(id)=>{
    
    let sql =
      "SELECT id_status,content,type, type_name as typename FROM status WHERE id_status=?";

    return new Promise((resolve, reject) => {
      connexion.query(sql,[id] , (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
},

}
 