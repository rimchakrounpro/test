//Imports
const statusQueries=require("./statusQueries")
const postQueries=require("../post/postQueries")
//Routes
module.exports = {
  store: async  (status) =>{
let idpub=null;
await postQueries.create({type:'status',idUser:status.idUser}).then(response=>{
	 idpub=response	   
 });
 if(idpub==null){
	 return {
				status: 400,
				payload: { success: false, msg: "erreur insersion post" }
 }}
  status={...status,idpub}
	  return statusQueries.create(status).then(response => ({
				status: 200,
				payload: { success: true, msg:"Enregistré avec succes "}
			}))
			.catch(err =>{
				postQueries.delete(idpub)
				 return({
				status: 400,
				payload: { success: false, msg: err }
				
			})
			});


  },
  getById:async(id)=>{
      return statusQueries.findOne(id).then(response => ({
				status: 200,
				payload: { success: true, data: response }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  },

 
};
