//Imports
const express = require("express");
const statusController = require("./statusController");
const multerMiddelware=require("../../middelware/multer")
const auth=require("../../middelware/auth")
const statusRouter = express.Router();
statusRouter.route("/").post(auth,statusController.store);
statusRouter.route("/video").post(auth,multerMiddelware.multVideo,statusController.store);
statusRouter.route("/image").post(auth,multerMiddelware.multImage,statusController.store);
statusRouter.route("/image/:name").get(auth,statusController.getImage);

statusRouter.route("/:id").get(statusController.getById);

// statusRouter.route("/:idpublicartion").get(statusController.getstatusByVideo);
// statusRouter.route("/:idpost").delete(statusController.delete);
module.exports =  statusRouter;
