//Imports
const statusService=  require("./statusService")
const jwtUtils=require("../../utils/jwt.utils")
const postQueries=require("../post/postQueries")

const fs=require("fs")


//Routes
module.exports = {
  store:function(req,res,){
    // verifier si la personne a ajouter un fichier
    if(req.file==undefined&&req.body.content===""){
      return res.status(400).json({ success:false,
        msg: "Aucune action..."
      });
    }
    let status={
        content:"",
        typename:"",
        type:"",
        idUser:req.idUser,
    }
    
status.content=req.body.content
if(req.file!==undefined){
status.typename=req.file.filename;
status.type=req.file.mimetype.split('/')[0]

    // passer l'objet video au service 
    statusService.store(status).then(result =>
      res.status(result.status).send(result.payload)).catch(err=>{
     // delete file async   
      fs.unlink(process.cwd()+ "/public/videos/"+req.file.filename)        
      });

}
else
{
  // passer l'objet video au service 
    statusService.store(status).then(result =>{
      res.status(result.status).send(result.payload)
    })
      
    

}

      
 
},
getById: (req,res) =>{  
  id=req.param("id");
  statusService.getById(id).then(result =>
      res.status(result.status).send(result.payload))
},
  getImage: (req, res)=>{

    // return res.sendFile(process.cwd()+ "/public/images/" + req.param('name'));
    try{
   // This line opens the file as a readable stream
  var readStream = fs.createReadStream(process.cwd()+ "/public/images/" + req.param('name'));

  // This will wait until we know the readable stream is actually valid before piping
  readStream.on('open', function () {
    // This just pipes the read stream to the response object (which goes to the client)
    readStream.pipe(res);
  });

  // This catches any errors that happen while creating the readable stream (usually invalid names)
  readStream.on('error', function(err) {
    res.end(err);
  });

}
catch(e){
 res.send({ success: false, msg:e })
}


  },

};
