//Imports
const userService=  require("./userService")
const { validationResult } = require('express-validator');

//Routes
module.exports = {
  profil:async(req,res)=>{
  userService.profil(req,res).then(result =>{
  
      res.status(result.status).send(result.payload)})
},
  getUserById: (req, res)=> {
    let id = req.param("id");
    if (id == null) {
      return res.json({ success: false, msg: "Champs obligatoires" });   
    }
    userService.info(id).then(result =>
      res.status(result.status).send(result.payload));
  },
  getPhoto: (req, res)=>{
    return res.sendFile(process.cwd()+ "/public/images/" + req.param('id'));
  },
 
  update: (req, res)=> {
     const  idUser  = req.param("id")
  if(req.param("id")==null)
  {
    

    return res.json({success:false,msg:"errors update user"})

  } 
   let firstName = req.body.firstName;
    if (firstName !== null)
      firstName =
        firstName.charAt(0).toUpperCase() +
        firstName.substring(1).toLowerCase();


    let lastName = req.body.lastName;
    if (lastName !== null)
      lastName =
        lastName.charAt(0).toUpperCase() + lastName.substring(1).toLowerCase();

    let metier = req.body.metier;
    if (metier !== null)
      metier =
        metier.charAt(0).toUpperCase() + metier.substring(1).toLowerCase();

    let country = req.body.country;
    let city = req.body.city;
    if (city !== null)
      city = city.charAt(0).toUpperCase() + city.substring(1).toLowerCase();

    let linkedin = req.body.linkedin;
    let facebook = req.body.facebook;
    let description = req.body.description;
    let newUrlPhoto=null;
     if (description !== null)
      description =
        description.charAt(0).toUpperCase() +
        description.substring(1).toLowerCase();

   
  
     let userData={
             idUser:idUser,
             firstName:firstName,
             lastName:lastName,
             metier:metier,
             country:country,
             city:city,
             linkedin:linkedin,
             facebook:facebook,
             description:description,
             photoUrl:newUrlPhoto
    } 
  userService.update(userData).then(result =>
      res.status(result.status).send(result.payload));
  },
  getUserByVideo:(req, res)=>{ 
     const id=req.param("id")
     userService.getUserByVideo(id).then(result =>
      res.status(result.status).send(result.payload));
  }
 
};
