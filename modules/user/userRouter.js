//Imports
const express = require("express");
const userController = require("./userController");
const fileController=require("../file/fileController")
const auth=require("../../middelware/auth")
const multerMiddelware=require("../../middelware/multer")
const validation=require("../../middelware/validation")
const path = require("path");
const userRouter = express.Router();
//routes
 userRouter.route("/me").get(auth,userController.profil);
 userRouter.route("/:id").get(auth,userController.getUserById);
 userRouter.route("/photo/:id").get(auth,userController.getPhoto);
 userRouter.route("/:id").put(auth,userController.update)
 userRouter.route("/video/:id").get(userController.getUserByVideo);
 userRouter.route("/photo").post(auth,validation.validate('fileUpload'),multerMiddelware.multImage,fileController.upload);
module.exports = userRouter;
