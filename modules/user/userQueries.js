const connexion = require("../../config/database");
const bcrypt = require("bcryptjs");

module.exports =  {
  register: (user) => {
      sql ="INSERT INTO users ( lastname, firstname,password, email) values(?,?,?,?)";
        return new Promise((resolve, reject) => {
            connexion.query(sql,[user.firstname,user.lastname,user.password,user.email],(err, rows) => {
             
                if (err) {
                    reject(err)}

                resolve(rows[0])
            })
        })

        },
  getUserByEmail: (email) => {
    
        let sqlQuery = "SELECT * FROM users WHERE email=?";

        return new Promise((resolve, reject) => {
            connexion.query(sqlQuery,[email],(err, rows) => {
             
                if (err) {
                    reject(err)}

                resolve(rows[0])
            })
        })
    },
  getUserById:(id)=>{
    console.log("id",id)
            
        let sqlQuery = "SELECT users.image, users.id_user, users.firstname, users.lastname,users.year_birth,addresses.id_address,addresses.city,addresses.country, users.description,users.contract,users.job,users.experience, users.email FROM users INNER JOIN addresses WHERE users.addresses_id_address=addresses.id_address AND users.id_user=?";
        return new Promise((resolve, reject) => {
            connexion.query(sqlQuery,[id],(err, rows) => {
                if (err) {
                    reject(err)
                  console.log("-------------",err)}
                resolve(rows[0])
            })
        })
    
},
update:(user)=>{

 let sql ="UPDATE users SET firstName=?,lastName=?, metier=?, city=?, linkedin=?, facebook=?, description=?, photoUrl=? WHERE idUser=?";

      return new Promise((resolve, reject) => {
            connexion.query(sql,[user.firstName,user.lastName,user.metier,user.country, user.city,user.linkedin,user.facebook,user.description,user.photoUrl,user.idUser],(err, rows) => {
                if (err) {
                    reject(err)}
                resolve(true)
            })
        })
  },
getUserByVideo:(idVideo)=>{
  
  let sql="SELECT users.id_user,users.firstname,users.lastname, users.country,users.city,users.facebook,users.linkedin,users.email,users.age,users.photoUrl FROM users INNER JOIN videos ON users.idUser= videos.idUser WHERE videos.idVideo=?"
       return new Promise((resolve, reject) => {
      connexion.query(sql, [idVideo] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
    
  }   ,
   getSkillsByUser:(idUser)=>{
 let sql="SELECT skills.title,skills.id_skill FROM skills INNER JOIN users_has_skills ON users_has_skills.skills_id_skill= skills.id_skill WHERE users_has_skills.users_id_user=?"
       return new Promise((resolve, reject) => {
      connexion.query(sql, [idUser] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
  }
}

