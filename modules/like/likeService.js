//Imports
const likeQueries=require("./likeQueries")
//Routes
module.exports = {
  store: async  (idPublication, idUser) =>{
	  // je verifie si l'utilisateur a deja liker la publication
	  const like= await likeQueries.getLike(idPublication,idUser);
	  if (!like){
		  const insertion= await likeQueries.create(idPublication,idUser);
		  if (insertion){
			  // increment dans la table videos
			  return likeQueries.updateNbLike(idPublication).then(response => ({
				status: 200,
				payload: { success: true, msg:"Enregistré avec succes "}
				}))
				.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
		}

}
	   
  },
 
  delete:async(idlike,idUserVideo,idUser)=>{
    return likeQueries.delete(idlike,idUserVideo,idUser).then(response => ({
				status: 200,
				payload: { success: true, msg:"supprimé avec success" }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			})); 
  },
 
};
