//Imports
const likeService=  require("./likeService")
const jwtUtils=require("../../utils/jwt.utils")

//Routes
module.exports = {
  store:(req,res)=>{
    const idPublication=req.param("id")
    const headerAuth = req.headers["authorization"];
    const idUser = jwtUtils.getUserId(headerAuth);
  likeService.store(idpublication,idUser).then(result =>
      res.status(result.status).send(result.payload))
},

delete:(req,res)=>{

  idlike=req.param("id");
  //id prop video
  idUserVideo=req.param("user");
  //get id user 
  headerAuth = req.headers["authorization"];
  idUser = jwtUtils.getUserId(headerAuth);
  console.log("demande de supp likeaire")

  likeService.delete(idlike,idUserVideo,idUser).then(result =>
      res.status(result.status).send(result.payload))
},
};
