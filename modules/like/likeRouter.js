//Imports
const express = require("express");
const likeController = require("./likeController");
const auth=require("../../middelware/auth")
const path = require("path");
const likeRouter = express.Router();
exports.router = (function() { 
likeRouter.route("/publication/:id").post(likeController.store);
likeRouter.route("/publication/:id").delete(likeController.delete);
  return likeRouter;
})();