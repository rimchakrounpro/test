const jwt = require('jsonwebtoken');
const dotenv = require("dotenv");
dotenv.config();
module.exports = (req, res, next) => {
  try{
 const { cookies, headers } = req;
 
    if (!cookies || !cookies.access_token) {
      return res.status(401).json({ errors: 'Missing token in cookie' });}
    const accessToken = cookies.access_token;

    if (!headers || !headers['x-xsrf-token']) {
      return res.status(401).json({errors: 'Missing XSRF token in headers' });
    }
     let xsrfToken = headers['x-xsrf-token'];

    const decodedToken = jwt.verify(accessToken,process.env.JWT_SIGN_SECRET);
    
    if (xsrfToken.replace(/['"]+/g, '') != decodedToken.xsrfToken) {
      return res.status(401).json({ errors: 'Bad xsrf token' });
    }
    req.idUser=decodedToken.idUser;
   next();
  }  catch (err) {
    return res.status(500).json({ errors: 'Internal error' });
  }

};