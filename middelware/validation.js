const { body,check} = require('express-validator')


exports.validate=(method)=>{
  switch (method) {
    
    case 'register': {
     return [ 
        body('firstname', 'Prénom obligatoire').not().isEmpty(),
        body('lastname', 'Nom obligatoire').not().isEmpty(),
        body('email', 'Email obligatoire').not().isEmpty(),
        body('password','Mot de passe obligatoire').not().isEmpty(),
        body('email', 'Email invalide').isEmail(),
        body("password", "Mot de passe doit contenir un melange de 8 lettres et chiffres").matches(/^(?=.*\d).{8,}$/, "i")
       ]   
    }
    case 'login':{
      return [
        body('email', 'Email obligatoire').not().isEmpty(),
        body('password','Mot de passe obligatoire').not().isEmpty(),
        body('email', 'Email invalide').isEmail(),

       ]
      
    }
     case 'fileUpload':{
       return[
                 body('myFile', ' obligatoire').not().isEmpty(),

        // custom error message that will be send back if the file in not a pdf. 
  ]

       }
      }
}