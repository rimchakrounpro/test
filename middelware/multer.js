const multer = require('multer');
const storage = multer.diskStorage({
  destination: (req, file, callback) => {
  
      const typeFile = file.mimetype.split('/')[0];
      if(typeFile==='image')
      {
        callback(null, 'public/images');
      }
    else if(typeFile=='video')
    {
      callback(null,'public/videos')}
    else {
      callback(new multer.MulterError("FILE_TYPE_NOT_SUPPORTED"))
    }
  },

  filename: (req, file, callback) => {

    const name = file.originalname.split(' ').join('_');
    const extension=file.mimetype.split('/').pop();
    callback(null,  Date.now()+name.toLowerCase());
  }
});

/* define filter */
const fileFilterVideo= (req, file, cb) => {

  const type=file.mimetype.split('/')[0]
  const extension=file.mimetype.split('/').pop();
  let acceptedFile=["mp4"]
  if (acceptedFile!==null && acceptedFile.includes(extension)
  ) {
    cb(null, true);
  } else {
    cb(new multer.MulterError("TYPE_FILE_NOT_SUPPORTED", file), false);
  }
};



/* define filter */
const fileFilterImage= (req, file, cb) => {
  
  const type=file.mimetype.split('/')[0]
  const extension=file.mimetype.split('/').pop();
  let acceptedFile=["png","jpeg","jpg"]
  if (acceptedFile!==null && acceptedFile.includes(extension)
  ) {
    cb(null, true);
  } else {
    cb(new multer.MulterError("TYPE_FILE_NOT_SUPPORTED", file), false);
  }
};
const uploadImage = multer({storage: storage,fileFilter:fileFilterImage, limits:{fileSize:7*1024*1204}}).single('myFile');

const uploadVideo = multer({storage: storage,fileFilter:fileFilterVideo, limits:{fileSize:15000*1024*1024}}).single('myFile');



  const multVideo=async (req,res,next)=>{

     return uploadVideo(req, res, (err)=> {

      if (err instanceof multer.MulterError)
       {
          return res.status(400).json({ message:err})
        }
        else if (err)
         {
           return res.status(400).json({message:err })
          }
     next()
  })
  }
  
  const multImage=async (req,res,next)=>{

     return uploadImage(req, res, (err)=> {

      if (err instanceof multer.MulterError)
       {
          return res.status(400).json({ message:err})
        }
        else if (err)
         {
           return res.status(400).json({message:err })
          }
     next()
  })
  }
module.exports  ={
  multImage,multVideo
}

 



  
  
 

