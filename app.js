
const fs = require('fs');
const path = require('path');
const https = require('https');
const express = require("express");
const userRouter = require("./modules/user/userRouter")
const authRouter=require("./modules/auth/authRouter")
const postRouter=require("./modules/post/postRouter")
const commentRouter=require("./modules/comment/commentRouter")
const trainingRouter=require("./modules/training/trainingRouter")
const statusRouter=require("./modules/status/statusRouter")
const likeRouter=require("./modules/like/likeRouter")
const cookieParser = require("cookie-parser");


 
const cors = require("cors");
const dotenv = require("dotenv");
const auth = require("./middelware/auth");
dotenv.config();
//Instantiate server
const server = express();
//port
const port = process.env.PORT;

// access to api
var corsOptions = {
    origin: "http://localhost:4200",
  optionsSuccessStatus: 200,
    credentials: true
}

server.use(cors(corsOptions));

server.use(express.json());
server.use(express.urlencoded({ extended: false }));
// cookie
server.use(cookieParser());

//Routes
server.use('/api/v1/',authRouter);
server.use('/api/v1/user',userRouter);
server.use('/api/v1/post',postRouter);
server.use('/api/v1/comment',commentRouter);
server.use('/api/v1/training',trainingRouter);
server.use('/api/v1/status',statusRouter);
server.use('/api/v1/cookies',authRouter)
// const key = fs.readFileSync(path.join(__dirname, 'certif', 'server.key'));
// const cert = fs.readFileSync(path.join(__dirname, 'certif', 'server.crt'));
// const options = { key, cert };
// 
server.listen(port, res => {
  console.log("Serveur fonctionne sur le port " );
});
