const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
const crypto = require('crypto');
const RefreshToken=require('../modules/auth/tokenQueries')
dotenv.config();
module.exports = {
  
  generateTokenForUser:(userData) =>{

     // Create CRSF token
    const xsrfToken = crypto.randomBytes(64).toString('hex');

    /* 
    Create  xsrfToken token, iduser, secret and expiration time */  
   let token=jwt.sign(
        { idUser: userData.id_user , xsrfToken:xsrfToken },
      process.env.JWT_SIGN_SECRET,
      { expiresIn: process.env.JWT_EXPIRE_IN }
    );
    
     return {token,xsrfToken};
  },



  
  parseAuthorization: function(authorization) {
    return authorization != null ? authorization.replace("Bearer ", "") : null;
  },
  getUserId: function(authorization) {
    let userId = -1;
    let token = module.exports.parseAuthorization(authorization);
    if (token != null) {
      try {
        let jwtToken = jwt.verify(token, process.env.JWT_SIGN_SECRET);
          if (jwtToken != null) 
          {
            userId = jwtToken.idUser;
          }
      } catch (err) {
        
      }
    }
    return userId;
  }
};
