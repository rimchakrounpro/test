module.exports = {
  apps : [{
    script: 'index.js',
    watch: 'api-trininginterview'
  }],

  deploy : {
    production : {
      user : 'userrim',
      host : '146.59.155.121',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:rimchakrounpro/test.git',
      path : '/var/www/test',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
