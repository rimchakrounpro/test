'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};


exports.up = function(db, callback) {
  
  db.createTable('status', {
    id_status: 
    {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
       foreignKey:
       {
         name: 'fk_status_post',           
         table: 'posts',
         rules:
         {
           onDelete: 'CASCADE',
           onUpdate: 'RESTRICT'
          },
          mapping: 'id_post'
        }
      },
      content:
      {
        type: 'string',
        length: 255,
      },
      type:
      {
        type: "enum('image','video','')",
      },
      typename:
      {
        type: 'string',
        length: 255,
      }
  
   
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('status', callback);
};

exports._meta = {
  "version": 1
};
