'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};


exports.up = function(db, callback) {
  
  db.createTable('reports', {
    post_id_post: 
    {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
      foreignKey:
        {
          name: 'pk_fk_report_post',           
          table: 'posts',
          rules:
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id_post'
        }
      },
    users_id_user: 
    {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
      foreignKey:
        {
          name: 'pk_fk_report_user',           
          table: 'users',
          rules:
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id_user'
        }
      },
      raison:
      {
        type: 'string',
        length: 255,
      },
    
     
        created_at:
        { 
        type: "timestamp", 
        notNull: "true", 
        defaultValue: new String('now()') 
      },
     
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('reports', callback);
};

exports._meta = {
  "version": 1
};
