'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};


exports.up = function(db, callback) {
  
  db.createTable('comments', {
    id_comment: 
    {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
      },
      content:
      {
        type: 'string',
        length: 255,
      },
      id_post: 
      {
        type: 'int',         
        length: 11,         
        notNull: true,  
        foreignKey:
        {
          name: 'fk_comment_post',           
          table: 'posts',
          rules:
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id_post'
        }
      },
     
        created_at:
        { 
        type: "timestamp", 
        notNull: "true", 
        defaultValue: new String('now()') 
      },
      updated_at:
      { 
        type: "timestamp", 
        notNull: "true", 
        defaultValue: new String('now()') 
      }
  
   
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('comments', callback);
};

exports._meta = {
  "version": 1
};
