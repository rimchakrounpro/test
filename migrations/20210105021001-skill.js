'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};


exports.up = function(db, callback) {
  
  db.createTable('skills', {
    id_skill: {
      type: 'int',
      primaryKey: true,
      autoIncrement: true,
      length:11,
      notNull: true,
    },
    title: {
      type: 'string',
      length: 45,
      notNull: true,
    },
   
    created_at:{ 
      type: "timestamp", 
      notNull: "true", 
      defaultValue: new String('now()') 
    },
    updated_at:{ 
      type: "timestamp", 
      notNull: "true", 
      defaultValue: new String('now()') 
    }
  
   
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('skills', callback);
};

exports._meta = {
  "version": 1
};
