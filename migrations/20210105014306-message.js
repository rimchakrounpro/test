'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};


exports.up = function(db, callback) {
  
  db.createTable('messages', {
    id_comment: 
    {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
      },
      content:
      {
        type: 'string',
        length: 255,
      },
      transmitter: 
      {
        type: 'int',         
        length: 11,         
        notNull: true,  
        foreignKey:
        {
          name: 'fk_user_1',           
          table: 'users',
          rules:
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id_user'
        }
      },
      receiver: 
      {
        type: 'int',         
        length: 11,         
        notNull: true,  
        foreignKey:
        {
          name: 'fk_user_2',           
          table: 'users',
          rules:
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id_user'
        }
      },
     
        created_at:
        { 
        type: "timestamp", 
        notNull: "true", 
        defaultValue: new String('now()') 
      },
     
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('messages', callback);
};

exports._meta = {
  "version": 1
};
