'use strict'; 
 var dbm; 
 var type;
  var seed;
    /**   * We receive the dbmigrate dependency from dbmigrate initially.   
     * * This enables us to not have to rely on NODE_PATH.   */
    exports.setup = function(options, seedLink)
      {   dbm = options.dbmigrate;   type = dbm.dataType;   seed = seedLink; 
      };
    exports.up = function(db, callback) 
    { 
           db.createTable('posts', 
           {     
             id_post: 
             { 
               type: 'int',
               primaryKey: true,
               autoIncrement: true,
               length:11,
               notNull: true,     
              },
              users_id_user:      
                 {         
                   type: 'int',         
                   length: 11,         
                   notNull: true,         
                   foreignKey: 
                   {           
                     name: 'fk_user_post',           
                     table: 'users',           
                     rules: 
                     {             
                       onDelete: 'CASCADE',             
                       onUpdate: 'RESTRICT'           
                      },           
                      mapping: 'id_user'         
                    }       
                  },
                  type:
                   {       
                     type: "enum('status','training')",
                    },
                     nb_like: 
              {        
                type: 'int',        
                length:11,             
              }, 
               nb_report: 
              {        
                type: 'int',        
                length:11,             
              },     
                   created_at:
                   { 
      type: "timestamp", 
      notNull: "true", 
      defaultValue: new String('now()') 
    },
    updated_at:{ 
      type: "timestamp", 
      notNull: "true", 
      defaultValue: new String('now()') 
    }
  
   
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('posts', callback);
};

exports._meta = {
  "version": 1
};
