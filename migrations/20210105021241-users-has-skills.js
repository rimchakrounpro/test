'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};


exports.up = function(db, callback) {
  
  db.createTable('users_has_skills', {
     users_id_user: {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
       foreignKey:
        {
          name: 'pk_fk_skills_user',           
          table: 'users',
          rules:
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id_user'
        }
    },
    skills_id_skill: {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
       foreignKey:
        {
          name: 'pk_fk_skills_user1',           
          table: 'skills',
          rules:
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id_skill'
        }
    }
   
  
   
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('users_has_skills', callback);
};

exports._meta = {
  "version": 1
};
