'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};


exports.up = function(db, callback) {
  
  db.createTable('notifications', {
    id_comment: 
    {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
      },
      content:
      {
        type: 'string',
        length: 255,
      },
      comments_id_comment: 
      {
        type: 'int',         
        length: 11,
        primaryKey: true, 
        notNull: true,  
        foreignKey:
        {
          name: 'fk_comment_notif',           
          table: 'comments',
          rules:
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id_comment'
        }
      },
     active:{
       type:"boolean",
       defaultValue:true

     },
     
        created_at:
        { 
        type: "timestamp", 
        notNull: "true", 
        defaultValue: new String('now()') 
      },
     
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('notifications', callback);
};

exports._meta = {
  "version": 1
};
