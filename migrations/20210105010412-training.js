'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};


exports.up = function(db, callback) {
  
  db.createTable('trainings', {
    id_training: {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
       foreignKey: 
                   {           
                     name: 'fk_training_post',           
                     table: 'posts',           
                     rules: 
                     {             
                       onDelete: 'CASCADE',             
                       onUpdate: 'RESTRICT'           
                      },           
                      mapping: 'id_post'         
                    }       
    },
    question:
                   {       
                     type: "enum('Parlez-moi de vous ?','Quels sont vos points forts ?','Pourquoi voulez-vous travailler dans notre entreprise ?','Qu’est-ce qui vous intéresse le plus et le moins dans le poste proposé ?','Que pouvez-vous nous apporter ?','Avez-vous d’autres propositions en cours ?')",
                    },
    videoname: {
      type: 'string',
      length: 255,
      notNull: true,
    }
  
   
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('trainings', callback);
};

exports._meta = {
  "version": 1
};
