'use strict'; 
 var dbm; 
 var type;
  var seed;
    /**   * We receive the dbmigrate dependency from dbmigrate initially.   
     * * This enables us to not have to rely on NODE_PATH.   */
    exports.setup = function(options, seedLink)
      {   dbm = options.dbmigrate;   type = dbm.dataType;   seed = seedLink; 
      };
    exports.up = function(db, callback) 
    { 
           db.createTable('users', 
           {     
             id_user: 
             { 
               type: 'int',
               primaryKey: true,
               autoIncrement: true,
               length:11,
               notNull: true,     
              },     
              firstname: 
              {
                type: 'string',
                length: 45,
                notNull: true,     
              },     
              lastname: 
              {       
                type: 'string',       
                length: 45,       
                notNull: true,     
              },     
              email: 
              {       
                type: 'string',       
                length: 45,       
                notNull: true,     
              },     
              password: 
              {       
                type: 'string',       
                length: 255,       
                notNull: true,     
              },     
              year_birth: 
              {        
                type: 'int',        
                length:11,             
              },     
              description: 
              {       
                type: 'string',       
                length: 255,       
                
                notNull: true,     
              },      
              experience:
               {       
                 type: "enum('Moins 1 an','1 an')",       
                 defaultValue: 'Moins 1 an'      
                },        
                image: 
                {       
                  type: 'string',       
                  length: 255,       
                  notNull: true,       
                  defaultValue: 'default.jpg',       
                },     
                contract:{        
                  type:"enum('CDI','CDD','Stage','Alternance')",     
                },      
                cities_id_city:      
                 {         
                   type: 'int',         
                   length: 11,         
                   notNull: true,         
                   foreignKey: 
                   {           
                     name: 'fk_user_city',           
                     table: 'cities',           
                     rules: 
                     {             
                       onDelete: 'CASCADE',             
                       onUpdate: 'RESTRICT'           
                      },           mapping: 'id_city'         
                    }       
                  },     
                   created_at:{ 
      type: "timestamp", 
      notNull: "true", 
      defaultValue: new String('now()') 
    },
    updated_at:{ 
      type: "timestamp", 
      notNull: "true", 
      defaultValue: new String('now()') 
    }
  
   
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('users', callback);
};

exports._meta = {
  "version": 1
};
