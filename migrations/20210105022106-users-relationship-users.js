'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};


exports.up = function(db, callback) {
  
  db.createTable('users_relationship_users', {
     users_id_user_request: {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
       foreignKey:
        {
          name: 'pk_fk_user1_user2',           
          table: 'users',
          rules:
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id_user'
        }
    },
     users_id_user_response: {
      type: 'int',
      primaryKey: true,
      length:11,
      notNull: true,
       foreignKey:
        {
          name: 'pk_fk_user2_user1',           
          table: 'users',
          rules:
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id_user'
        }
    },
      active:{
       type:"boolean",
       defaultValue:false

     },
     
     created_at:{ 
      type: "timestamp", 
      notNull: "true", 
      defaultValue: new String('now()') 
    },
   
  
   
  }, function(err) {
    if (err) return callback(err);
    return callback();
  });
};
exports.down = function(db, callback) {
  db.dropTable('users_relationship_users', callback);
};

exports._meta = {
  "version": 1
};
